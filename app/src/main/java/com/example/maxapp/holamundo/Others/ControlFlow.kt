package com.example.maxapp.holamundo.Others

import android.util.Log

/**
 * Created by Zavala on 21/11/2017.
 */

class ControlFlow{


    private fun showCase1(){
        //If
        val num1 = 5
        val num2 = 10

        if (num1 > num2){
            Log.w("CONTROL-FLOW-1", "num1 es mayor que num2")
        }
        if (num2 > num1){
            Log.w("CONTROL-FLOW-1", "num1 es mayor que num2")
        }
    }


    private fun showCase2(){
        //If else
        val num1 = 5
        val num2 = 10

        if (num1 > num2){
            Log.w("CONTROL-FLOW-2", "num1 es mayor que num2")
        }else{
            Log.w("CONTROL-FLOW-2", "num1 es mayor que num2")
        }

        //Como expresión
        if (num1 > num2) Log.w("CONTROL-FLOW-2", "num1 es mayor que num2") else Log.w("CONTROL-FLOW-1", "num1 es mayor que num2")
        val result = if (num1 > num2) num1 else num2
    }


    private fun showCase3(){
        //When, lo que sería Switch en Java
        val x = 1

        // Con argumento (x)
        when(x){
            1 -> Log.w("CONTROL-FLOW-3", "x == 1") //case 1
            2 -> Log.w("CONTROL-FLOW-3", "x == 2") //case 2
            else -> Log.w("CONTROL-FLOW-3", "x es otro número.") //case default, no es obligatorio
        }

        when(x){
            0,1 ->  Log.w("CONTROL-FLOW-3", "x == 0 o x == 1") //case 0 y 1
        }

        //Sin argumento.
        when{
            (x % 2 == 0) ->  Log.w("CONTROL-FLOW-3", "Número es par") //
            (x % 2 == 1) ->  Log.w("CONTROL-FLOW-3", "Número es impar") //
        }

        //Sin argumento y devolviendo un valor.
        var esPar = when{
            (x % 2 == 0) ->  true
            else -> false
        }
    }


    private fun showCase4(){
        // Bucle For
        val numbers = arrayOf(1,2,3,4,5)

        for(number in numbers) Log.w("CONTROL-FLOW-4 ",number.toString())

        // Especificando el tipo
        for(number: Int in numbers){
            Log.w("CONTROL-FLOW-4 ",number.toString())
        }

        // Con índices.
        for((index, number) in numbers.withIndex()){
            Log.w("CONTROL-FLOW-4 ", "$index: $number")
        }
    }


    private fun showCase5(){
        //While and do while
        var x = 10

        while (x > 0){
            Log.w("CONTROL-FLOW-5 ", x--.toString())
        }

        do {
            Log.w("CONTROL-FLOW-5 ", "Primera y única iteración.")
        }while (x > 0)

    }
    fun showCases(){
        showCase1()
        showCase2()
        showCase3()
        showCase4()
        showCase5()
    }
}