package com.example.maxapp.holamundo.Others

import android.util.Log

/**
 * Created by Zavala on 20/11/2017.
 */

//En Kotlin, todo es un objeto.
//No hay tipos básicos y no existe void. Si algo no devuelve nada, está devolviendo Unit Object.
//Las variables pueden ser mutables o inmutables, usaremos inmutables siempre que sea posible. (var vs val)
//Los tipos, son definidos después del nombre de la variable, con dos puntos y espacio. (var nombre: tipo)
class Variables{

    private var variable0 = 1
    private var variable1 = 1.toByte()

    private var variable2 = -123
    private var variable3 = 2421671531
    private var variable4 = 1.1.toFloat()
    private var variable5 = 1.9
    private var variable6 = 'a'
    private var variable7 = "a"
    private var variable8 = true
    private var variable9 = arrayOf(1,2,3,4,5)
    private var variable10 = arrayOfNulls<Int>(10) // [null, null] solo acepta Int
    private val variable11 = "Esta variable es read-only, inmutable, constante, etc."


    //Show Case para Byte
    private fun showCase1(){
        Log.w("VARIABLE-0","¿Es variable 0 un Integer? -----> " + (variable0 is Int) + " ¿Por qué no un Byte?")
        Log.w("VARIABLE-1","¿Es variable 1 un Integer? -----> " + (variable1 is Byte))
    }

    //Show Case para Int
    private fun showCase2(){
        Log.w("VARIABLE-2","Es un valor Integer")
        variable2 = Int.MAX_VALUE
    }

    //Show Case para Long
    private fun showCase3(){
        Log.w("VARIABLE-3","¿Es un valor Long? --> " + (variable3 is Long))

    }

    //Show Case para Float
    private fun showCase4(){
        Log.w("VARIABLE-4","¿Es un valor Float? --> " + (variable4 is Float))

    }
    //Show Case para Double
    private fun showCase5(){
        Log.w("VARIABLE-5","¿Es un valor Float? --> " + (variable5 is Double))

    }
    //Show Case para char
    private fun showCase6(){
        Log.w("VARIABLE-6","¿Es un valor Float? --> " + (variable6 is Char))

    }

    //Show Case para Double
    private fun showCase7(){
        Log.w("VARIABLE-7","¿Es un valor Float? --> " + (variable7 is String))

        // Strings Literals
        variable7 = "Hola mundo \n Adiós Mundo" //Java's style
        variable7 = """
                        Hola mundo
                        Adiós mundo
                    """                         //Kotlin's style

        // String Templates
        var points = 9
        var maxPoints = 10
        variable7 = "Hola, tengo " + points + " puntos sobre " + maxPoints //Java's styles
        variable7 = "Hola, tengo $points puntos sobre $maxPoints"         //Java's styles

        //Si se requiere realizar operaciones
        variable7 = "Hola, tengo ${points * 10} puntos sobre ${maxPoints * 10}"         //Java's styles


    }
    //Show Case para Bool
    private fun showCase8(){
        Log.w("VARIABLE-8","¿Es un valor Float? --> " + (variable8 is Boolean))

    }
    //Show Case para Array<Int>
    private fun showCase9(){
        Log.w("VARIABLE-9","¿Es un valor Float? --> " + (variable9 is Array<Int>))

    }
    //Show Case para Array<Int>
    private fun showCase10(){
        Log.w("VARIABLE-10","¿Es un valor Float? --> " + (variable10 is Array<Int?>))

        variable9[3]
        variable10[4]
        variable10 = arrayOfNulls(3) //Se permite porque es un valor mutable.
        variable10 = arrayOf(1,2,3,4,5)

        //variable10[0].toFloat() //Indica error porque puede ser nulo.

        //Safe Calls --> En caso de ser null, devuelve null de nuevo.
        variable10[0]?.toFloat() //Lo permite porque se está indicando que el array puede contener nulos.

        //Otra forma de realizar lo anterior, sería...
        variable10[0]?.toFloat() ?: "Error" //Esto se llama, Elvis Operator -- En caso de ser null, devuelve "Error"

        //La última forma en la que se puede realizar esto es...
        variable10[0]!!.toFloat() ?: "Error" //The !! Operator -- Cuando estamos seguros que no es nulo al 100%. Si lo fuera, lanza un NullException

    }

    fun showCases(){
        showCase1()
        showCase2()
        showCase3()
        showCase4()
        showCase5()
        showCase6()
        showCase7()
        showCase8()
        showCase9()
        showCase10()
    }
}
