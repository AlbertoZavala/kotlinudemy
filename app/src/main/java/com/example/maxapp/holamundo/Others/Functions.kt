package com.example.maxapp.holamundo.Others

import android.util.Log

/**
 * Created by Zavala on 27/11/2017.
 */


class Functions {

    private fun showCase1(){
        // Funciones, parámetros y Unit.

        fun function1(){}
        val result1 = function1()

        fun function2() : Unit {}
        val result2 : Unit = function2()

        // Los parámetros de las funciones necesitan especificar su tipo, siempre.
        fun function3(param1: String, param2: String): String {
            return "$param1, $param2"
        }
        val result3 = function3("String 1", "String 2")

        fun function4(param: Int = 5){}
        fun function5(param: Int?){}
    }

    private fun showCase2(){
        // Funciones con funciones como parámetros(callback) y llamadas por lambda

        fun printSum(num1: Int, num2: Int,printer:(result:Int) -> Unit){
            printer(num1 + num2)
        }
        
        printSum(5,5){ result ->
            Log.w("FUNCTIONS-2","Lambda expression printing the sum result (10) => $result")
        }

        // Si el callback recibe solo un parámetro, podemos omitir ese "result" ==> it
        printSum(12,12){
            Log.w("FUNCTIONS-2","Lambda expression printing the sum result () => $it")
        }

        fun printSum2(num1:Int,num2:Int,printer: (result:Int, param1:Int,parame2:Int) -> Unit){
            printer(num1+num2,num1,num2)
            //Log.w("FUNCTIONS-2","Lambda expression printing the sum result (10) => $it")
        }
    }

    private fun showCase3(){
        // Named arguments
    }

}
