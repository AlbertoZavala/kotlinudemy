package com.example.maxapp.holamundo.Others

/**
 * Created by Zavala on 20/11/2017.
 */

class Nullable{
    /**
     * Operadores relacionados con la nulabilidad:
     * - only-safe(?)
     * - non-null asserted (!!)
     *
     * Sistema de tipado de Kotlin está pensado para eliminar de nuestro código el NullPointerException.
     * Los 4 únicos posibles casos para NPE son:
     *
     * 1) Una llamada explícita al error NullPointerException().
     * 2) Uso del operador !!
     * 3) Código externo Java
     * 4) Alguna inconsistencia de datos en relación a la inicialización.
     */

    // La declaración de las siguientes variables es equivalente.
    private lateinit var variable1: String //lateinit indica que se declarará después el valor de la variable. Si no se inicializa, mostrará un NPE
    private var variable2: String? = null //En este caso el desarrollador se hace responsable de que la variable sea null.

    private fun showCase1(){
        throw NullPointerException()
    }

    private fun showCase2(var1 : String?){
        var1.toString() //Devuelve null en caso de que var1 sea null
        var1!!.toString() //Devuelve un NPE en caso de que var1 sea null
    }

    private fun showCase3(){
        NullPointerExJava.getNPE() //Devuelve un  NPE.
    }

    private fun showCase4(){
        variable1.length //Devuelve un NPE porque variable1 no ha sido inicializada y se ha marcado como tal.

        //variable2.length //Marca un error porque se da cuenta que no ha sido inicializada.
        //Para lo anterior, se puede declarar como...
        variable2?.length //Indica que podría ser null
        variable2!!.length //El desarrollador asegura que será null. Si no, mostrará un NPE.
    }

    fun showCases(){
        showCase1()
        showCase2("Ejemplo")
        showCase3()
        showCase4()
    }

}