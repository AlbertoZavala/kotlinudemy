package com.example.maxapp.holamundo.Others

/**
 * Created by Zavala on 21/11/2017.
 */

// Para hacer una clase "extendible" o "heredable" tenemos que marcarlo como open.
// En java, es open por defecto y en Kotlin es final por defecto, así que no puede ser extendida sin el uso de 'open'


open class AccessModifiers{

    // private      --sólo visible dentro de la clase
    // protected    --Sólo visible dentro de la clase y sus subclases (herencia)
    // public       --Visible desde dentro de la clase, de sus subclases y desde fuera

    val prop1 = "Public vy default"
    private val prop2 = "Marked as private"
    protected val prop3 = "Marked as protected"

    protected fun showCase(){
        val test = AccessModifiers()
        test.prop3
        this.prop1

        prop1

    }
}

class AccessModifiersChild : AccessModifiers(){

    private fun showCaseX(){
        super.showCase()
        prop1
        this.prop1
        // prop2 // Por ser private no puede ser accesible.
        prop3

    }

}

class AnotherClass{
    private fun showCase(){
        val test = AccessModifiers()
        test.prop1

    }
}

